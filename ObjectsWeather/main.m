//
//  main.m
//  ObjectsWeather
//
//  Created by Ahmed Elashker on 4/27/16.
//  Copyright © 2016 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
