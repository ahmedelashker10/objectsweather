//
//  AppDelegate.h
//  ObjectsWeather
//
//  Created by Ahmed Elashker on 4/27/16.
//  Copyright © 2016 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

